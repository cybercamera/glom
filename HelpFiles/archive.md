<title>Glom - Archive Help</title>

Archiving Webpages in Glom 
==========================

## Overview

One of glom's cornerstone features is the ability to capture web-pages and keep them available long-term in a document archive. glom attempts to store as much of the web objects which are necessary to allow you render these archived web-pages as possible. glom will also make a thumbnail of the current web-page you're looking at, and render a PDF copy as well.


## How to Archive a Web Page

### Alt+a

You can archive the web-page you're currently viewing by either hitting the Alt key and pressing the 'a' key. glom's archive facility requires the *wget* command to be present on your system. wget is normally installed on Linux, but if not, install it using your standard installation process.

### Command line

You can also archive the current web-page by using:

    :tab archive

from the command-line.

### Searching Through Archives (Full Text Search)

You can search through the archives using a full-text search (fts) facility. You can provide a list of search words, and glom will show you each archive entry which has such matching search words, along with some other text, to give you context. Example:

    :archive fts bike trike cycle bicycle

This will find all archived documents which contain any of these words. These search results will be in the form of a report that you can scroll through, etc. 

You can also issue SQL queries to fine-tine your search criteria. glom uses the SQLite3 engine and the Full Text Search 5 (FTS5) extension for this purpose. You can therefore issue any valid 'WHERE' clauses as part of your search parameters. 

### Showing Archive Data

Just as with *history* and with *bookmarks*, glom's archives give you the ability to show the main database table - which contains information about the archived document -in a grid. For example:

    :archive show voldemort

This will show you all archive data which has the word 'voldemort' in either the document title or the original URL from which the document came. You can filter the data or even delete individual archives while in the grid by using the *Del* key. This will remove the data from the document information table, from the full-text-search table and also the document tree.

You can also search the archive informstion table for more complex arrangements of keywords, such as:

    :archive show voldemort hates potter

Which will only show you archives where these three keywords all appear in the document title or the URL.

Finally, you can query the database directly via SQL. glom uses the SQLite3 engine, so any legitimate commands should work. For example:

    :archive show "SELECT * FROM archive where document_title LIKE '%Horizons%' AND url_string LIKE '%nasa.gov%';"
    
Which will show you all entries from the archives where the document has Horizons somewhere in the title and it's a document most likely sourced from nasa.gov

For more information on SQLite3's SQL syntax, visit: [https://sqlite.org/docs.html]

### Deleting Archive Data

Besides deleting individual archives using the data grid, you can also issue *delete* commands which can specify a larger group of documents to remove. For example:

    :archive delete pascal
    
Will remove all archived documents (and document information data and full-text-search data related to those documents) which have *pascal* in their document title or URL. Much llike with the *show* command, delete actions can be made across an array of items, using matches or even direct SQL statements. For example, 


    :archive delete "DELETE FROM archive where document_title LIKE '%Marty%' AND url_string LIKE '%backtothefuture.com%';

Will delete all entries in the archives which include 'Marty' in the title and backtothefuture.com as part of the URL.

    :archive delete freeze schwarzenegger
    
Will delete all entries in the archives which have any combination of 'freeze' and 'schwarzenegger' in the titles or URLs.

## Managing Archived Web Pages

Archived pages are stored in the *archive store*, which is found as a sub-directory off the user's *~/.glom/archive* directory. The structure for this archive is a separate directory tree for each archived page, denoted by the date & time at which the archive took place, as well as the full directory tree structure of the URL from which that web-page came. Here's an example

Archiving:

    https://www.mydomain.com/content/monday_briefing/index.html
    
Will result in an archive which looks something like this:

    ~/.glom/archive/2021_06_08_21_48_24/www.mydomain.com/content/monday_briefing/index.html
    
A PDF of that web-page will also be dropped here:

  ~/.glom/archive/2021_06_08_21_48_24/document.pdf

