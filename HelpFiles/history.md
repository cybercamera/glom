<title>Glom - URL History Help</title>

URL History in Glom 
===================

## Overview

glom stores (long-term) information on all web-sites visited. This consists of the URL and a date-time stamp of visiting. glom can quickly retrieve URLs from history, using either a reverse-history search, or using command-line commands which will present the URL history as a database search result.


## ^h

This will bring up the history reverse-history search mode in the command line. Enter text that you recall being part of the website URL or the web-page title, and glom will try to find it.

## Command line

From the command-line, you can enter:

    :history {sub-commamd} {parameters}
    
The various sub-commands are:

delete, show, wipe

* delete - deletes the nominated history. The parameter is the history ID or as nominated query parameter.
* show - shows all of the history in a database view or as nominated query parameter.
* wipe - wipes all history from the database.


### Showing URL History Data

Just as with *archives* and with *url_history*, glom's URL history facility give you the ability to show the main database table - which contains information about the URL pages which have been visited -in a grid. For example:

    :history show voldemort

This will show you all URL history data which has the word 'voldemort' in either the document title or the original URL from which the document came. You can filter the data or even delete individual URL history while in the grid by using the *Del* key. This will remove the data from the document information table, from the full-text-search table and also the document tree.

You can also search the URL history information table for more complex arrangements of keywords, such as:

    :history show voldemort hates potter

Which will only show you URL history where these three keywords all appear in the document title or the URL.

Finally, you can query the database directly via SQL. glom uses the SQLite3 engine, so any legitimate commands should work. For example:

    :history show "SELECT * FROM url_history where document_title LIKE '%Horizons%' AND url_string LIKE '%nasa.gov%';"
    
Which will show you all entries from the URL history where the document has Horizons somewhere in the title and it's a document most likely sourced from nasa.gov

For more information on SQLite3's SQL syntax, visit: [https://sqlite.org/docs.html]

### Deleting URL History Data

Besides deleting individual URL history using the data grid, you can also issue *delete* commands which can specify a larger group of documents to remove. For example:

    :history delete pascal
    
Will remove all URL history documents (and document information data and full-text-search data related to those documents) which have *pascal* in their document title or URL. Much llike with the *show* command, delete actions can be made across an array of items, using matches or even direct SQL statements. For example, 


    :history delete "DELETE FROM url_history where document_title LIKE '%Marty%' AND url_string LIKE '%backtothefuture.com%';

Will delete all entries in the URL history which include 'Marty' in the title and backtothefuture.com as part of the URL.

    :history delete freeze schwarzenegger
    
Will delete all entries in the URL history which have any combination of 'freeze' and 'schwarzenegger' in the titles or URLs.


### Show URL by date

You can also show the URL history with date parameters. Either specific dates (such as '2021-08-10 22:45:35') or representational dates (such as 'hour' or 'week') will work. For example:

    :history show #week# 
    
Will show you all URL history for the preceding week. #hour#, #day#, #week#, #month#, #year# will all work. Alternatively, you can specify explicit dates, in ISO date format:

    :history show #"2021-08-20 22:30:14"#
    
Will show you all URL history between the nominated date, and now. **Note:** the double-quotes surrounding the date.


## Usage

### Delete

Usage:
	:history delete 223

Will delete the history item whose ID is 223

	:history delete gmail

Will delete the history items which match the gmail wildcard.

### Show

Usage:
	:history show 

Will show all history items in a database view.

	:history show gmail

Will show all history items which match the gmail wildcard.

### Wipe

Usage:
	:history wipe 

Will completely wipe the history database.

