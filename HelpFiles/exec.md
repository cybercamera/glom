<title>Glom - Exec Help</title>

Spawning Shell Commands
=======================

## Overview

glom is able to spawm shell commands and display the STDOUT from those commands. Further, it can pipe both the text and HTML of the current (in-view) web-browser tab. The exec commands (shell exec, text-pipe and html-pipe) provide this functionality.


## | Exec Command

This will spawn a shell and execute the shell command that you nominate. In theory, this can be any shell command, but if you want to capture the output from that command and have it presented to you in a browser tab in glom, the command stream will need to send output to standard-out.

Usage:
	| {command} {parameters...}

Examples:
	|ls -lart
	|ping -c 5 www.abc.net.au
	|cat ~/file1.txt
	|mkdir -p ~/Music/2020/Newfile
	|locate -i godot

## % Text Pipe Exec Command

This will spawn a shell and execute the shell command that you nominate, piping through to that command chain the *text* contents of the current browser tab. In theory, this can be any shell command, but if you want to capture the output from that command and have it presented to you in a b
rowser tab in glom, the command stream will need to send output to standard-out.

Usage:  
        % {command} {parameters...}

Examples:
        %grep -v bob | grep -i 
	%egrep -i '^(linux|unix)' filename
	%grep '^\.[0-9]'

## $ HTML Pipe Exec Command

This will spawn a shell and execute the shell command that you nominate, piping through to that command chain the *HTML* contents of the current browser tab. In theory, this can be any shell command, but if you want to capture the output from that command and have it presented to you in a browser tab in glom, the command stream will need to send output to standard-out.

Usage:  
        $ {command} {parameters...}

Examples:
	$sed  -e  '/Austria/d' 
        $grep -v bob | grep -i  
        $egrep -i '^(linux|unix)' filename
        $grep '^\.[0-9]'

