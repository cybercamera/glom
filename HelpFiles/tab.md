<title>Glom - Tab Help</title>

Managing Browser Tabs in Glom 
=============================

## Overview

Like almost all modern browsers, glom presents multiple web-browsers as tabs. These tabs can be spawned, closed, archived, searched, etc, via the command line.


## Command line

To acess the functionality to manage tabs, you can enter:

    :tab {sub-commamd} {parameters}
    
The various sub-commands are:

new, close, reload, extract, save, back, forward

* new - spawns a new browser tab.
* close - closes the current browser tab.
* reload - reloads the current browser tab.
* extract - extracts the current browser tab TEXT to a file.
* save - saves the current browser HTML to a file.
* archive - archives the current browwser tab to the archive storage, along with all web objects needed to render it in future.
* back - goes back to previous web page.
* forward - goes forward again to the web page you just moved back from.
* open - opens a TEXT or HTML file into the current browser tab.


### New

Usage:
	:tab new
	
Will open up a new tab, opening your default home-page in the process.

### Close

Usage:
	:tab close

Will close the current browser tab.

### Reload 

Usage:

	:tab reload

Will reload the current browser tab.

### Extract

Usage:
	:tab extract /home/conz/Documents/web_file.txt

Will save the text from the current browser to the nominated file-name parameter. *Note:* glom has file-completion, which you access by providing part of the fully-qualified file-path, then pressing the *Tab* key.

### Save

Usage:
	:tab save /home/conz/Documents/web_page.html

Will save the HTML source text from the current browser to the nominated file-name parameter. *Note:* glom has file-completion, which you access by providing part of the fully-qualified file-path, then pressing the *Tab* key.

### Archive

Usage:
	:tab archive

Will download all the files necessary to render this page and place them in the archive store. These will them be available via the *arhive* command.

### Back

Usage:
	:tab back

Will navigate you back one web-page URL.


### Forward

Usage:
	:tab forward

Will navigate you forward one web-page URL, assuming you've just come *back* from one.


### Open

Usage:
	:tab open /home/conz/Documents/web_page.html

Will open the text document (and if it's in HTML, render it) within the current tab.
