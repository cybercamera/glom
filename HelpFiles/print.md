<title>Glom - Print Help</title>

Printing in Glom 
================

## Overview

glom is able to print directly to the default printer from the command line, or you can open a printer configuration dialog to help you select other printers and to tweak the settings.


## Control+p

This will open the standard print dialog. Choose your options and print your web page.

## Command line

From the command-line, you can enter:

    :print {parameters}
    
to print your document. The parameters are:

* Space separated list of page numbers (*e.g, 1 4 11 17*)
* Space separated list of page ranges (*e.g, 2-5 8-12 22-30*)
* Or a combination of these two (*e.g, 2 8 13 17-24 33-40*)

To print the entire document, provide the *print* command with no options:

    :print

### Printing to a File

You can also print the web page to a (PDF) file, by supplying a filename as a parameter:

    :print my_web_document.pdf

will print the whole document to the file *my_web_document.pdf*, which will be placed in the *printout* directory, which can be set in configuration.

