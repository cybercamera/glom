<title>Glom - Config Help</title>

Configuration in Glom 
=====================

## Overview

glom stores configuration in text files, located (by default) in the user's *\~\/.glom* directory. These text files can obviously be edited by hand. They can also be modified within glom, by use of the configuration command, per:

    :config set {setting} = "value"
 
Or shown, per:

    :config get {setting}
    
(When getting the configuration of *{setting}*, its vlue is shown in the statusline.)

You can *tab-complete* {setting} parameters, as well as the config sub-commands. We can demonstrate by using the config *get* sub-command:

    :config get 
    
and then press in the Tab key. This will then cycle through the list of config settings. You can also enter the first few letters of the settings parameter, and the Tab key will present matching options.
    
## Available Settings

The following are the configurable settings, with some default settings:

    [General]
      user_agent_string="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36"
      external_browser="firefox"
      download_helper="wget"
      glom_home="/.glom/"
      download_directory="/Downloads/"
      printout_directory="/Downloads/"
screenshot_directory="/Downloads/"
glom_homepage_url="https://news.ycombinator.com/"
n_max_rows_to_retrieve=10000
user_agent_string_google_com="ELinks / 0.9.3(textmode; Linux 2.6.9 - kanotix - 8 i686; 127 x41)"


### User Agent

This allows you to change the decault way the browser presents itself to web-sites. By default, glom uses the Chromium user-agent string. This can be set manually via:

    :config set user_agent_string = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36"

Other strings that could be of use are:

* elinks -> **:config set** *user_agent_string = "ELinks / 0.9.3(textmode; Linux 2.6.9 - kanotix - 8 i686; 127 x41)"*
* firefox -> **:config set** *user_agent_string = "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0"*

### External Browser

glom was never intended to be a complete replacement for your existing browser. If it is, then great. But if it's not, you can switch to another (perhaps more mainstream) browser by hitting ^Enter on your keyboard. This will then launch the current browser URL in your designated alternate browser. By default, *firefox* is the nominated alternate external browser. You can change this however, for example, via:

    :config set external_browser="chromium"

### Download Helper

glom uses existing tools to facilitate the downloading of whole web-pages, for example, to archive storage. By default, *wget* is used for this purpose, which means that you'll need to have that installed. Alternatively, you could use another tool, for example, *curl*. To make this happen, do:

    :config set download_helper=curl

### glom Home

By default, glom stores its configuration, database, cookies and archive in a directory off the user's $HOME directory: *\~/.glom/* You can change this default via:

    :config set glom_home=my_glom_home_dir

### User Agent per-Domain

You can also associate a user-agent string to a specific domain, to give you more granular control of how you present yourself on the web:

    :config set user_agent_per_domain["*.gmail.com"] = "ELinks / 0.9.3(textmode; Linux 2.6.9 - kanotix - 8 i686; 127 x41)

This ensures you offer the *elinks* user-agent string when retrieving all pages off the *\*.gmail.com* domain range.*

## Glom Config File

The glom config file lives in *\~/.glom/glom.conf* If you're careful, you can edit it by hand. Changes wont be affected until you restart glom. Also note, you should only make manual changes when you're not running glom, as it will write out its current configurations to the file upon exit.
