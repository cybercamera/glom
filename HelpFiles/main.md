<title>Glom - Main Help</title>

Welcome to the Glom Web-browser! 
================================


## Overview

glom is the graphical browser for anyone who'd prefer to use a text-based browser like lynx [http://lynx.browser.org/] or elinks [http://elinks.or.cz/] but with basic levels of graphical display and modern browser functionality, like Javascript. glom will never fully-replace your mainstream browser, but if you're someone who has a very heavy focus on information content and less of a focus on glitz or web-based applications, glom could become part of your web-access toolkit. 

glom has a variety of unique features. First, it's keyboard and command line focused, being the only graphical browser that I'm aware of which has a functional command-line, along with the ability to *exec* shell functions, pipe web-content to external applications, provide tab-completion for commands, sub-commands and file-pathing, etc. glom also has a focus on information management, storage and retrieval. As such, it provides the ability to capture and archive web-content and exposes a database that allows you to extract information via full SQL queries. Yes, web-search engines exist, but glom allows you to search your own archive where you know that everything present is what you've prevoiusly deemed important.

## Purpose

glom was designed for geeks and hackers. Unlike the mainstream browsers, the glom codebase was built to be both simple and using simple development tools, while remaining performant. As such, it's not a daunting task for someone to familiarise themselves with its codebase and learn how to hack or extend it. At the end of all things, glom was built by its developer to cater for his tastes and requirements. Feel free to make it do the same for you.

## Webview Keyboard Shortcuts


[[
General
-------
--
Command Line Only
-----------------
--
Browser Only
------------
--
Mouse Gestures
--------------
==
**Esc** - Command Mode
--
**Up-arrow** - Previous Command
--
**Alt+Left-arrow** - Go Back Page
--
**Drag Middle-Button Left** - Go Back Page
==
**^q** - Quit Appliction
--
**Down-arrow** - Next Command
--
**Alt+Right-arrow** - Go Forward Page
--
**Drag Middle-Button Right** - Go Forward Page
==
**^t** - New Tab
--
**^u** - Clear Command Line
--
**Alt+Up-arrow** - Close Current Tab
--
**Drag Middle-Button Up** - Close Current Tab
==
**Alt+q** - Close Current Tab
--
**^W** - Delete Previous Word
--
**Alt+Down-arrow** - Spawn New Tab
--
**Drag Middle-Button Down** - Spawn New Tab
==
**^x** - Cut
--
**^Left-arrow** - Go Back Word
--
**^a** - Select All
==
**^c** - Copy
--
**^Right-arrow** - Go Forward Word
--
==
**^v** - Paste
--
**^h** - Reverse History Search
--
==
**^r** - Reload Page
--
**^r** - Reverse Command Search
--
==
**^p** - Print
--
**^b** - Bookmarks Search
--
==
**^+** - Zoom in 
--
**^a** - Start of Command Line
--
==
**^-** - Zoom out
--
**^e** - End of Command Line
--
==
**^=** - zoom default
--
**^Shift-A** - Select all text
--
==
**^f** or **^/** - Search Current Page 
--
--
==
**^g** or **^F3** - Repeat Search
--
--
==
**^o** - File Open (w/- Tab Completion)
--
--
==
**Enter** - Opens Focused Link
--
--
==
**^Enter** - Opens Current URL in External Browser
--
--
==
**Meta+Enter** - Downloads the Current Website to the Downloads directory
--
--
==
**^F4** - Close Current Tab
--
--
==
**Alt+F4** - Quit Application
--
--
==
**^Shift** - Set Focus to Current Browser
--
--
==
**^Tab** - Switch to Tabs/Cycle Through Tabs
--
--
==
**^**```` - Show Current Browser URL in Command Line
--
--
==
**F11** - Switch Full-screen Mode
--
--
==
**Alt+b** - Add Current Page to Bookmarks
--
--
==
**Del** - While in the URL History | Commands | Bookmarks | Archives Grid, Deletes the Selected Row 
--
--
==
**^,** - Show Browser History
--
--
==
**Alt+m** - Establish Username and/or Password Credemtials for the Current Site, via the Password Manager
--
--
==
**Alt+u** - Inject the Stored Username for this Site
--
--
==
**Alt+p** - Inject the Stored Password for this Site
--
--
==
**^s** - Save the Current Webpage as a HTML File
--
--
==
**^l** - Save the Current Webpage as a Text File
--
--
==
**^SPACE** - Switches Focus to Browser
--
--
==
**Alt+a** - Adds the Current Website to the glom Archive
--
--
==
**Alt++** - Embiggen Text Size
--
--
==
**Alt+-** - Debiggen Text Size
--
--
==
**Alt+=** - Normalise Text Size
--
--
==
**Alt+n** - Open a Note for this URL
--
--
==
**^\\** - View Page Source
--
--
==
**Alt+h** - Sets the Current URL as the Default Homepage
--
--
==
**Meta+e** - Enables Error Logging
--
--
**Meta+y** - Enables Memory Profile Logging
--
--
]]


## [Command Line Usage]

### URL Entry Mode

A URL can be entered into the command-line directly, e.g:

    www.example.com
    https://www.example.com/
    http://www.example.com/
  
Alternatively, you can perform a web-search by pre-fixing a letter (as shorthand) which denotes your favourite search method. The currently available set of searches are:

* **d** example - Search for *example* on **DuckDuckGo**
* **g** example - Search for *example* on **Google**
* **m** 14 example street, exampletown, Mystate - Search for this address on Google **Maps**
* **dm** 14 example street, exampletown, Mystate - Search for this address on **DuckDuckgo** **Maps**
* **gm** 14 example street, exampletown, Mystate - Search for this address on **Google** **Maps**
* **om** 14 example street, exampletown, Mystate - Search for this address on **OpenStreetMap** **Maps** (Note: OSM takes a long time to render the homepage.)
* **b** example - Search *example* in Google **Books**
* **s** example - Search *example* in Google **Scholar**


### Command Mode
To enter command mode, hit the Esc key, followed by a colon (i.e, **:**), just like you would in Vi/Vim. You can then enter a command and any optional sub-commands or paramters, e.g:

    : <command> <sub-command> parameters
    
In command mode, you can open new tabs, get help on specific parts of the application, run queries on URL and command history, run queries on the glom document archive, run queries on the bookmarks database, get and set configuration settings, execute shell commands, change view/zoom settings, etc.

#### Commands:

* **tab** *sub-command* *parameters* {new, close, reload, extract, save, back, forward, etc} (e.g, *:tab extract > tab3*)
* **config** {set, get}
* **quit** *parameters* (e.g, *:quit *+15mins*)
* **help** *parameters* {main, commands, search, archive, etc} (e.g, *:help commands*)
* **history** *sub-command* *parameters*   {show, delete, wipe }  (e.g, *:history today, yesterday, last week, last year, <date-range>*)
* **set** *parameters*  (e.g, *:set user_agent_string="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36"*, *:set external_browser="firefox"*)
* **get** *parameter* (e.g, *default_zoom=120%*)
* **bookmarks** *sub-command* *parameters* (e.g, *:bookmarks view where URL match reg-ex*, *:bookmarks wipe*)
* **cookies** *sub-command* *parameters* {show, delete, wipe} (e.g, *:cookies view \*.google.com*)
* **screenshot** *parameters* (e.g, *:screenshot filename.png*)
* **archive** *sub-command* *parameters* {show, delete, wipe, fts} (e.g, *:archive show \*neutrinos*)
* **notes** *sub-command* *parameters* (e.g, *:notes view where URL match reg-ex*, *:notes wipe*)

You can find more information about the commands and options in the Commands help file (viz: *:help commands*)


### Shell Exec Mode

In shell exec mode, you can spawn background operating-system processes which allow you to do things like run commands and redirect the results in a new browser tab for review. To access the shell exec mode hit Esc to get into the command-line and enter the vertical pipe/bar character (i.e, **|**).

Once in shell exec mode, you can issue pretty much any shell command you like. Make sure to wrap your shell command in double-quotes, to differentiate any actions you want taken at the shell level and any you want taken in the browser level.

For example, the following command:

    | "execute shell command shell" 

Will execute the given command in the shell of choice and return the results in a new browser tab.

Here other examples:

    | "ls -laRt"                          'runs the command and puts output into a new tab. 
    | "nslookup www.example.com"          'runs the comnand and puts the output into a new tab.
    | "find . / | grep myname > file.txt" 'runs the command and puts the output into a text file at the operating system level.


### HTML Pipe Mode

In HTML pipe mode, you can spawn background operating-system processes which allow you to do things like pipe the HTML from the current browser tab to the shell, run commands and redirect the results in a new browser tab for review. To access the shell exec mode hit Esc to get into the command-line and enter the vertical pipe/bar character (i.e, **$**).

Once in shell exec mode, you can issue pretty much any shell command you like. Make sure to wrap your shell command in double-quotes, to differentiate any actions you want taken at the shell level and any you want taken in the browser level.

For example, the following command:

    $ "execute shell command shell"

Will pipe the current web browser HTML text to the shell and execute the given command in the shell of choice and return the results in a new browser tab..

Here other examples:

    $ grep com | grep h1                  'Pipes the current browser HTML and looks for lines which have "com" and "h1". 
    $ elinks -dump                        'Pipes current browser HTML through the elinks text browser and presents the output into a new tab.

### Text Pipe Mode

In Text pipe mode, you can spawn background operating-system processes which allow you to do things like pipe the text from the current browser tab to the shell, run commands and redirect the results in a new browser tab for review. To access the shell exec mode hit Esc to get into the command-line and enter the vertical pipe/bar character (i.e, **%**).

Once in shell exec mode, you can issue pretty much any shell command you like. Make sure to wrap your shell command in double-quotes, to differentiate any actions you want taken at the shell level and any you want taken in the browser level.

For example, the following command:

    % "execute shell command shell"

Will pipe the current web browser text to the shell and execute the given command in the shell of choice and return the results in a new browser tab..

Here other examples:

    % grep com | grep h1                'Pipes the current browser text through the 'gre' command and looks for lines which have "com" and "h1". 
    % sed '/[0-9]\{5,\}$/q'             'Pipes current browser text through the 'sed' command and presents the output into a new tab.
    % grep '^linux'                     'Pipes current browser text through the 'grep' command and presents the output into a new tab.


### Search Mode

    / parameters 

Will search for that text in the current browser tab. *if* the text is part of a url, pressing Enter will open the URL in the current browser view. Pressing Ctrl + Enter will spawn a new tab.

Regex may be added.


### Tab Completion

glom comes with command-line tab completion. While in command mode, you can tab-complete a partial command or sub-commands, or hit tab at the command prompt to get a list of commands, or you can hit tab after providing a command to get a list of its sub-commands. To dis-ambiguate from various commands which have similar first few characters, you need to provide enough characters. 

Here other examples:

    : co            'Hitting tab now will provide suggestions for both the 'config' and the 'cookies' command 
    : con           'Hitting tab now will autocomplete the 'config' command 
    : coo           'Hitting tab now will autocomplete the 'cookies' command 
    : config s      'Hitting tab now will autocomplete the 'set' sub-command of the 'config' command

You can also perform tab completion on configuration settings. Therefore, if you want to cycle through all of the various *user-agent* strings you've set, you can enter:

    : config get user_ag    'Hitting tab will now cycle through the user_agent_string settings in the configuration file.

### Content Extraction

    : tab extract ~/Documents/filename.txt 'Extract the text from the HTML and save as file

## Per Web Domain Username/Password Authentication

glom gives you username & password authentication functionality via a password manager. To establish the username and/or password for a domain:

+ Firstly, if you've already established authentication details for any domain, you will need to 'login' to glom once the browser starts by supplying the password manager password.
+ Bring up the domain/web-site you want to establish credentials for in a browser tab.
+ Hit Alt+m (for password **M**anager)
+ If you've not already established an unlocking password for the password manager, you will be prompted to do so now. Enter this in the coloured text-entry box which appears.
+ You will then be prompted to supply a *username* for this domain. Enter this in the coloured text-entry box which appears. Then press Enter.
+ You will then be prompted to supply a *password* for this domain. Enter this in the coloured text-entry box which appears. Then press Enter.

To access/use any stored usernames or passwords which have been prevously established, navigate to that domain/web-site:

+ Firstly you will have have had 'unlock' the password manager by supplying the password manager password once glom starts.
+ Click or tab into the Username field of the web Page
+ Hit Alt+U to add the stored username into the text field.
+ Click or tab into the Password field of the web Page
+ Hit Alt+U to add the stored password into the text field.

Passwords are stored using the "AES-256-CBC" encrypt/decrypt cipher which comes as part of OpenSSL. There are plenty of others available if you're more paranoid than I am. All encryptions are also salted for increased security.

All usernames and passwords are available in the *\~/.glom/glom.secrets* file. Feel free to delete them as needed.

## Archiving Webpages

One of glom's cornerstone features is the ability to capture web-pages and keep them available long-term in a document archive. glom attempts to store as much of the web objects which are necessary to allow you render these archived web-pages as possible. glom will also make a thumbnail of the current web-page you're looking at, and render a PDF copy as well.

## Adding Notes to Webpages

glom can store and quickly retrieve notes for every URL you visit. You can add a note to a web-page, archive or local file:/// document through a key-binding, and you can retrieve it just as easily. Notes, much like bookmarks etc, can also be found and managed through command and the dataview grid. Any URL which has an associated note, is designated by a little yellow 'post-it' note icon shown in the tab.

## Viewing History, Archives, Bookmarks and Notes via the Datagrid

Each of History, Archives, Bookmarks and Notes data is available to you via a tabular dataview. Each can be shown via a:

    :history show
    :bookmarks show
    :notes show
    :archive show
    
command, along with any corresponding search parameters. Once the datagrid is up, you navigate through the list of results presented, deleting items with the Del key, or opening items by clicking on them or pressing the Enter key.

## Private Mode Browsing

You can switch glom into private mode browsing by hitting ^Shift+n. While in private mode, none of the URLs you visit, none of the URLs you enter and none of the cookies are stored. For securty reasons, once you switch the browser into private mode browsing, you can't switch back into normal mode. Either restart glom or launch a new instance.

## Viewing Page

### Zooming In and out

You can use the ^+ key to zoom in to a webpage (i.e, make it bigger) and ^- to zoom out (i.e, to make it smaller.)

### Increasing and Decreasing Webpage Text Size

You can use the Alt++ key to increase the default font size for webpage, and Alt+- to decrease it.

### Reset Web Page Zoom and Default Font Sizes

To get the zoom levels back to normal (i.e, 100%), use the ^= key-binding. Similarly, to get the text size defaults back to normal, use the Alt+= key-binding.

### Viewing source

To view the HTML source of a web-page, use the ^\\ keybinding.

## Invoking from the Command Line

You can invoke glom from the command-line (shell) in the following use-cases:

Launching glom with a specific URL

    Usage:
    $ glom https://mysampleurl.com/file.HTML
    
Dumping the HTML from a specific URL to STDOUT

    Usage:
    $ glom https://mysampleurl.com/file.HTML -d,--dump
    
Getting help at the command-line

    Usage:
    $ glom -h,--help
    
Dumping the text from a specific URL webpage to STDOUT

    Usage:
    $ glom https://mysampleurl.com/file.HTML -e,--extract
    

Getting version information

    Usage:
    $ glom -V,--version


Fetch the nominated URL and print it to the default printer

    Usage:
    $ glom https://mysampleurl.com/file.HTML -p,--print

Fetch the nominated URL, generate a PDF from it and save it to the specified file

    Usage:
    $ glom https://mysampleurl.com/file.HTML -P,--pdf


## Known Limitations

glob is obviously not in the same league with respect to features of rendering functionality as mainstream browsers like Firefox or Chromium-based browsers. Among its current (and possibly long-term) limitations are:

* Inability to SSL handshake with web-servers running self-signed certificates.
* Inability to render some complex or Javascript-heavy web pages. 

On this last point, your mileage may vary. You may want to try changing the user_agent_string for each of these sites. While some wont help to improve the experience on those re-calcitrant sites, others might. Good luck.

## Getting More Help

glom ships with separate help files for the *archive*, *bookmarks*, *config*, *cookies*, *exec*, *history*, *notes*, *print*, *screenshot* and *tab* functional areas. To show these help files, issue the help command with the requisite help file. For example:

    :help history
    :help archive
    :help print
    :help tab
    
