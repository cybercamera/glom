<title>Glom - Bookmarks Help</title>

Bookmarks in Glom 
=================

## Overview

glom can store and quickly retrieve bookmarks, using either a reverse-history search, or using command-line commands which will present the bookmarks as a database search result.


## ^b

This will bring up the bookmarks reverse-history search mode in the command line. Enter text that you recall being part of the bookmarked website or the web-page title, and glom will try to find it.

## Alt+b

This will bookmark the current web-page.

## Command line

From the command-line, you can enter:

    :bookarks {sub-commamd} {parameters}
    
The various sub-commands are:

delete, show, wipe

* delete - deletes the nominated bookmark. The parameter is the bookmark ID or as nominated query parameter.
* show - shows all of the bookarks in a database view or as nominated query parameter.
* wipe - wipes all bookarks from the database.


### Showing Bookmark Data

Just as with *archives* and with *url_history*, glom's bookmark facility give you the ability to show the main database table - which contains information about the bookmarked document -in a grid. For example:

    :bookmark show voldemort

This will show you all bookmark data which has the word 'voldemort' in either the document title or the original URL from which the document came. You can filter the data or even delete individual bookmarks while in the grid by using the *Del* key. This will remove the data from the document information table, from the full-text-search table and also the document tree.

You can also search the bookmark information table for more complex arrangements of keywords, such as:

    :bookmark show voldemort hates potter

Which will only show you bookmarks where these three keywords all appear in the document title or the URL.

Finally, you can query the database directly via SQL. glom uses the SQLite3 engine, so any legitimate commands should work. For example:

    :bookmark show "SELECT * FROM bookmarks where document_title LIKE '%Horizons%' AND url_string LIKE '%nasa.gov%';"
    
Which will show you all entries from the bookmarks where the document has Horizons somewhere in the title and it's a document most likely sourced from nasa.gov

For more information on SQLite3's SQL syntax, visit: [https://sqlite.org/docs.html]

### Deleting Bookmark Data

Besides deleting individual bookmarks using the data grid, you can also issue *delete* commands which can specify a larger group of documents to remove. For example:

    :bookmark delete pascal
    
Will remove all bookmarked documents (and document information data and full-text-search data related to those documents) which have *pascal* in their document title or URL. Much llike with the *show* command, delete actions can be made across an array of items, using matches or even direct SQL statements. For example, 


    :bookmark delete "DELETE FROM bookmarks where document_title LIKE '%Marty%' AND url_string LIKE '%backtothefuture.com%';

Will delete all entries in the bookmarks which include 'Marty' in the title and backtothefuture.com as part of the URL.

    :bookmark delete freeze schwarzenegger
    
Will delete all entries in the bookmarks which have any combination of 'freeze' and 'schwarzenegger' in the titles or URLs.


## Usage

### Delete

Usage:
	:bookmark delete 223

Will delete the bookmark whose ID is 223

	:bookmark delete gmail

Will delete the bookmarks which match 'gmail' wildcard.

### Show

Usage:
	:bookmark show 

Will show all bookmarks in a database view.

	:bookmark show gmail

Will show all bookmarks which match the 'gmail' wildcard.

### Wipe

Usage:
	:bookmark wipe 

Will completely wipe the bookmarks database.
