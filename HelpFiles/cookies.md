<title>Glom - Cookies Help</title>

Cookies in Glom 
===============

## Overview

glom stores and can managed cookies. 


## Command line

From the command-line, you can enter:

    :cookies {sub-commamd} {parameters}
    
The various sub-commands are:

delete, show, wipe

* delete - deletes the nominated cookie. The parameter is the cookie domain ID or as nominated by the query parameter.
* show - shows all of the cookies in the cookie-jar, or as nominated by the query parameter.
* wipe - wipes all cookies from the cookie-jar.

### Delete

Usage:
	:cookies delete .33across.com

Will delete the cookies whose domain is .33across.com

	:cookies delete google*

Will delete the cookies which match the google* wildcard.

### Show

Usage:
	:cookies show 

Will show all cookies in a database view.

	:cookies show gmail*

Will show all cookies which match the gmail* wildcard.

### Wipe

Usage:
	:cookies wipe 

Will completely wipe the cookie-jar. You can also delete the cookie-jar file (id est, *~/.glom/cookies/glom.cookies*) manually.

## Wildcard Matches

[[
Wildcard Character
------------------
--
Matches
-------
--

==
**\***
--
Any number of any character.
--
==
**?**
--
Any single character.
--
==
**\[abc\]**
--
Any character between the brackets.
--
==
**\[x-y\]**
--
Any character in the interval.
--
==
**\[^x-y\]**
--
Any character not in the interval.
--
==
**space**
--
Any number of spaces or characters with an ASCII code lower than 32.
--
==
**{aaa,bbb,...}**
--
One of the strings between the square brackets. The strings are separated by commas.
--
==
**\\x**
--
Escape the wild-card character
--
==
]]