<title>Glom - Notes Help</title>

Notes in Glom 
=================

## Overview

glom can store and quickly retrieve notes for every URL you visit. You can add a note to a web-page, archive or local file:/// document through a key-binding, and you can retrieve it just as easily. Notes, much like bookmarks etc, can also be found and managed through command and the dataview grid. Any URL which has an associated note, is designated by a little yellow 'post-it' note icon shown in the tab.



## Alt+n

This will bring up a new (or previously added note) for the current URL in view. The URL has to be an exact match, meaning that "https://theconversation.com/au" and "https://theconversation.com/au/" are treated as two separate 'notable' entities, so some care and attention is necessary. 

Alt+n will show you a note (looking kinda-sorta like a post-it note). The note will show you the HTML-rendered content of the markdown notes, if any. By default, each note spawns with some boiler-plate primer on markdown, in case you're not across it. To access the actual markdown, double-click the note. This will show you the markdown in a text editor. Change the note content as you see fit, then double-click on the markdown editor to be shown the HTML-rendered content once again. You can close the note using the standard window decoration on your desktop-environment, or you can hit the *Esc* key. Each note closed like this will automatically update (i.e, save) the new note content in the notes data-strore.


## Command line

From the command-line, you can enter:

    :notes {sub-commamd} {parameters}
    
The various sub-commands are:

delete, show, wipe

* delete - deletes the nominated note(s). The parameter is the note ID or a nominated query parameter.
* show - shows all of the notes in a database view or a nominated query parameter.
* wipe - wipes all notes from the database.


### Showing Note Data

Just as with *archives* and with *url_history*, glom's notes facility give you the ability to show the main database table - which contains information about the noted document -in a grid. For example:

    :notes show voldemort

This will show you all note data which has the word 'voldemort' in either the document title or the original URL from which the document came. You can filter the data or even delete individual notes while in the grid by using the *Del* key. This will remove the data from the document information table, from the full-text-search table and also the document tree.

You can also search the note information table for more complex arrangements of keywords, such as:

    :notes show voldemort hates potter

Which will only show you notes where these three keywords all appear in the document title or the URL.

Finally, you can query the database directly via SQL. glom uses the SQLite3 engine, so any legitimate commands should work. For example:

    :notes show "SELECT * FROM notes where document_title LIKE '%Horizons%' AND url_string LIKE '%nasa.gov%';"
    
Which will show you all entries from the notes where the document has Horizons somewhere in the title and it's a document most likely sourced from nasa.gov

For more information on SQLite3's SQL syntax, visit: [https://sqlite.org/docs.html]

### Deleting Note Data

Besides deleting individual notes using the data grid, you can also issue *delete* commands which can specify a larger group of documents to remove. For example:

    :notes delete pascal
    
Will remove all noted documents (and document information data and full-text-search data related to those documents) which have *pascal* in their document title or URL. Much llike with the *show* command, delete actions can be made across an array of items, using matches or even direct SQL statements. For example, 


    :notes delete "DELETE FROM notes where document_title LIKE '%Marty%' AND url_string LIKE '%backtothefuture.com%';

Will delete all entries in the notes which include 'Marty' in the title and backtothefuture.com as part of the URL.

    :notes delete freeze schwarzenegger
    
Will delete all entries in the notes which have any combination of 'freeze' and 'schwarzenegger' in the titles or URLs.


## Usage

### Delete

Usage:
	:notes delete 223

Will delete the note whose ID is 223

	:notes delete gmail

Will delete the notes which match 'gmail' wildcard.

### Show

Usage:
	:notes show 

Will show all notes in a database view.

	:notes show gmail

Will show all notes which match the 'gmail' wildcard.

### Wipe

Usage:
	:notes wipe 

Will completely wipe the notes database.
