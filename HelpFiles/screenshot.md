<title>Glom - Screenshot Help</title>

Taking Screenshots in in Glom 
=============================

## Overview

glom is able to take screenshots of the current web-page. No borders or application widgets are included in these screenshots, just the web-page. The screenshots are all saved (along with date and time-stamps) to a directory nominated as a configuration options (the *screenshots_directory*).


## Alt+S

This will take a screenshot and store it in the scopen the standard print dialog. Choose your options and print your web page.

## Command line

From the command-line, you can enter:

    :screenshot
    
to take a screenshot of the current web-page.

## Accessing the screenshots

The screenshots are stored in the directory nominated by the *screenshots_directory* which is a sub-directory in the user's *.glom/* directory found in *$HOME*. You can set an alternate sub-directory using the *config* set command, per:

	:config set screenshots_directory=new_screenshots_directory


